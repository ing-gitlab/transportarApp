package com.transportar.demo.transportar.Entities;

import android.graphics.drawable.Drawable;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import com.transportar.demo.transportar.R;

public class Notificacion {
    private int Tipo;
    private int IdNotificacion;
    private Drawable IconoNotificacion;
    private String ID;
    private String Fecha;
    private String Hora;
    private String Contenido;
    private String Tag;

    public Notificacion() {
    }

    public Notificacion(int tipo, int idNotificacion, Drawable iconoNotificacion, String ID, String fecha, String hora, String contenido, String tag) {
        Tipo = tipo;
        IdNotificacion = idNotificacion;
        IconoNotificacion = iconoNotificacion;
        this.ID = ID;
        Fecha = fecha;
        Hora = hora;
        Contenido = contenido;
        Tag = tag;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int tipo) {
        Tipo = tipo;
    }

    public int getIdNotificacion() {
        return IdNotificacion;
    }

    public void setIdNotificacion(int idNotificacion) {
        IdNotificacion = idNotificacion;
    }

    public Drawable getIconoNotificacion() {
        return IconoNotificacion;
    }


    public void setNotificationIcon(Activity f,View v) {
        switch (this.Tipo) {
            case 1:
                IconoNotificacion = f.getResources().getDrawable(R.drawable.ic_action_notificacion);
                v.setBackgroundColor(Color.rgb(0, 160, 198));
                break;
            case 2:
                IconoNotificacion = f.getResources().getDrawable(R.drawable.ic_action_notificacion);
                v.setBackgroundColor(Color.rgb(223, 0, 36));
                break;

            case 3:
                IconoNotificacion = f.getResources().getDrawable(R.drawable.ic_action_notificacion);
                v.setBackgroundColor(Color.rgb(255, 210, 0));
                break;

            default:
                IconoNotificacion = f.getResources().getDrawable(R.drawable.ic_action_notificacion);
                v.setBackgroundColor(Color.rgb(0, 160, 198));
                break;
        }
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public String getContenido() {
        return Contenido;
    }

    public void setContenido(String contenido) {
        Contenido = contenido;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }
}
