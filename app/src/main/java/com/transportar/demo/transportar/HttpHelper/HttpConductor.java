package com.transportar.demo.transportar.HttpHelper;


import android.app.Activity;

import com.transportar.demo.transportar.Entities.Conductor;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpPost;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.body.JSONObjectBody;

import org.json.JSONException;
import org.json.JSONObject;

public class HttpConductor implements IHttpHelper<Conductor>{
    public HttpListener listener = null;

    public void setObjectListener(HttpListener listener) {
        this.listener = listener;
    }

    @Override
    public void Post(String url, Conductor conductor) {
        AsyncHttpPost post = new AsyncHttpPost(url);
        JSONObjectBody jsonBody = new JSONObjectBody(BuildUserJson(conductor));
        post.setBody(jsonBody);

        AsyncHttpClient.getDefaultInstance().executeJSONObject(post,
                new AsyncHttpClient.JSONObjectCallback() {

                    @Override
                    public void onCompleted(Exception arg0, AsyncHttpResponse arg1, JSONObject res) {
                        try{

                            if(listener != null) {
                                listener.onDataLoaded(res);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void Get(String url) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    public void PostLogin(String url, String token, Conductor conductor) {
        try {

            AsyncHttpPost post = new AsyncHttpPost(url);

            JSONObject json = new JSONObject();
            json.put("correo", conductor.getCorreo());
            json.put("password", conductor.getContrasena());

            JSONObjectBody jsonBody = new JSONObjectBody(json);
            post.setHeader("Authorization", "Bearer " + token);
            post.setBody(jsonBody);

            AsyncHttpClient.getDefaultInstance().executeJSONObject(post, new AsyncHttpClient.JSONObjectCallback() {
                @Override
                public void onCompleted(Exception arg0,final AsyncHttpResponse response,JSONObject json) {
                    try{
                        final JSONObject objson = json;
                        if(listener != null) {
                            listener.onDataLoaded(objson);
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void Get(String url, Conductor conductor ) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    private JSONObject BuildUserJson(Conductor conductor){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject();
            jsonObject.put("deviceId", conductor.getDeviceId());
            jsonObject.put("nombres", conductor.getNombre());
            jsonObject.put("apellidos", conductor.getApellido());
            jsonObject.put("telefono", conductor.getTelefono());
            jsonObject.put("correo", conductor.getCorreo());
            jsonObject.put("password", conductor.getContrasena());
            jsonObject.put("confirmPassword", conductor.getConfirmarContrasena());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return jsonObject;
    }
}
