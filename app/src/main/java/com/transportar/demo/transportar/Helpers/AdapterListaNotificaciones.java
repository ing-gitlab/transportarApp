package com.transportar.demo.transportar.Helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.transportar.demo.transportar.Entities.ItemNotificacion;
import com.transportar.demo.transportar.R;

import java.util.ArrayList;

public class AdapterListaNotificaciones extends BaseAdapter {
    private Activity activity;
    private ArrayList<ItemNotificacion> zonaArrayList = new ArrayList();

    public AdapterListaNotificaciones(Activity activity, ArrayList<ItemNotificacion> lista) {
        this.activity = activity;
        this.zonaArrayList = lista;
    }

    @Override
    public int getCount() {
        return zonaArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return zonaArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
//        return zonaArrayList.get(position).getId();
        return 1;
    }

    private String ExtractHora(String fecha) {
        String time = fecha;
        try{
            time = fecha.split(",")[1];
        }catch(Exception ex){}
        return time;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vista = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vista = inf.inflate(R.layout.item_notificacion, null);
        }

        ItemNotificacion itemNotificacion = zonaArrayList.get(position);

        LinearLayout ic_notificacion = (LinearLayout) vista.findViewById(R.id.iconNotificacion);
        ic_notificacion.setBackgroundDrawable(itemNotificacion.getIcono());

        TextView titulo = (TextView) vista.findViewById(R.id.txtTitulo);
        titulo.setText(itemNotificacion.getTitulo());

        TextView descripcion = (TextView) vista.findViewById(R.id.txtdescripcion);
        descripcion.setText(itemNotificacion.getDescripcion());

        TextView hora = (TextView) vista.findViewById(R.id.txtHora);
        hora.setText(ExtractHora(itemNotificacion.getFecha()));

        TextView avatar = (TextView) vista.findViewById(R.id.txtAvatar);
        avatar.setText(itemNotificacion.getAvatar());

        return vista;
    }
}
