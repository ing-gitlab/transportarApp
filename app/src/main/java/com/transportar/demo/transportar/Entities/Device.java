package com.transportar.demo.transportar.Entities;

/**
 * Created by hjimenez on 03/27/2017.
 */

public class Device {
    String RegistrationId;
    double latitude;
    double longitude;

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }

    public void setRegistrationId(String registrationId) { RegistrationId = registrationId; }

    public double getLatitude() { return latitude; }

    public double getLongitude() { return longitude; }

    public String getRegistrationId() { return RegistrationId; }
}
