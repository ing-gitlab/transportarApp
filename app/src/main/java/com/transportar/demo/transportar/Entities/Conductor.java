package com.transportar.demo.transportar.Entities;


public class Conductor {
    String Id;
    String DeviceId;
    String Nombre;
    String Apellido;
    String Telefono;
    String Correo;
    String Contrasena;
    String ConfirmarContrasena;
    String Estado;

    public Conductor() {
    }

    public Conductor(String id, String deviceId, String nombre, String apellido, String telefono, String correo, String contrasena, String confirmarContrasena, String estado) {
        Id = id;
        DeviceId = deviceId;
        Nombre = nombre;
        Apellido = apellido;
        Telefono = telefono;
        Correo = correo;
        Contrasena = contrasena;
        ConfirmarContrasena = confirmarContrasena;
        Estado = estado;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String contrasena) {
        Contrasena = contrasena;
    }

    public String getConfirmarContrasena() {
        return ConfirmarContrasena;
    }

    public void setConfirmarContrasena(String confirmarContrasena) {
        ConfirmarContrasena = confirmarContrasena;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }
}
