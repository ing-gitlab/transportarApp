package com.transportar.demo.transportar.Helpers;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.transportar.demo.transportar.R;


/**
 * Created by thiago on 28/03/2015.
 */

public class LRutasSalida {
    private int id;
    private String titulo;
    private int tipo;
    private Drawable icono;
    private String barrios;
    private String hora;
    private int estado;

    public LRutasSalida(int id, String titulo, String barrios, String hora, int estado, int tipo, Activity activity) {
        this.titulo = titulo;
        this.barrios = barrios;
        this.hora = hora;
        this.estado = estado;
        this.tipo = tipo;

        switch (tipo) {
            case 1:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_azul));
                break;
            case 2:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_amarillo));

                break;
            case 3:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_verde));

                break;
            case 4:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_rojo));

                break;
            default:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_rojo));
                break;
        }
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getBarrios() {
        return barrios;
    }

    public void setBarrios(String barrios) {
        this.barrios = barrios;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drawable getIcono() {
        return icono;
    }

    public void setIcono(Drawable icono) {
        this.icono = icono;
    }


}
