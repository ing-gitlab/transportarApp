package com.transportar.demo.transportar.Entities;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.transportar.demo.transportar.R;

public class PuntoReferencia {
    private String id;
    private String nombre;
    private int numero;
    private int estado = 0;
    private int agregado;
    private Drawable icono;

    public PuntoReferencia() {
    }

    public PuntoReferencia(String nombre, int numero, Activity activity) {
        String _nombre = formatearNombre(nombre);;
        this.nombre = _nombre;
        this.numero = numero;
        setIconoInicial(_nombre, activity);
    }

    public int getAgregado() {
        return agregado;
    }

    public void setAgregado(int agregado) {
        this.agregado = agregado;
    }

    public int getEstado() {return estado;}

    public void setEstado(int estado) {this.estado = estado;}

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {this.nombre = nombre;}

    public int getNumero() {return numero;}

    public void setNumero(int numero) {this.numero = numero;}

    public Drawable getIcono() {return icono;}

    public void setIcono(Drawable icono) {this.icono = icono;}

    public String getInicial(String palabra){
        return palabra.substring(0, 1).toUpperCase();
    }

    private String formatearNombre(String cadena){
        //convierte la primera letra de una palabra en mayuscula
        char[] caracteres = cadena.toCharArray();
        caracteres[0] = Character.toUpperCase(caracteres[0]);

        return new String(caracteres);
    }

    public void setIconoInicial(String nombre, Activity activity) {
        char inicial = getInicial(nombre).substring(0, 1).charAt(0);
        switch (inicial){
            case 'A':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_a));
                break;
            case 'B':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_b));
                break;
            case 'C':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_c));
                break;
            case 'D':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_d));
                break;
            case 'E':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_e));
                break;
            case 'F':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_f));
                break;
            case 'G':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_g));
                break;
            case 'H':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_h));
                break;
            case 'I':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_i));
                break;
            case 'J':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_j));
                break;
            case 'K':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_k));
                break;
            case 'L':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_l));
                break;
            case 'M':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_m));
                break;
            case 'N':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_n));
                break;
            case 'O':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_o));
                break;
            case 'P':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_p));
                break;
            case 'Q':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_q));
                break;
            case 'R':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_r));
                break;
            case 'S':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_s));
                break;
            case 'T':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_t));
                break;
            case 'U':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_u));
                break;
            case 'V':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_v));
                break;
            case 'W':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_w));
                break;
            case 'X':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_x));
                break;
            case 'Y':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_y));
                break;
            case 'Z':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_z));
                break;
            case '-':
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_selected));
                break;
            default:
                setIcono(activity.getResources().getDrawable(R.drawable.back_color_a));
                break;
        }
    }

}
