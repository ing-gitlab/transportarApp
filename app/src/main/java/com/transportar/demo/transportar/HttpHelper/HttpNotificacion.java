package com.transportar.demo.transportar.HttpHelper;


import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpPost;
import com.koushikdutta.async.http.AsyncHttpPut;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.body.JSONObjectBody;
import com.transportar.demo.transportar.Entities.Conductor;
import com.transportar.demo.transportar.Entities.Notificacion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpNotificacion implements IHttpHelper<Notificacion>{
    public HttpListener listener = null;

    public void setObjectListener(HttpListener listener) {
        this.listener = listener;
    }

    @Override
    public void Post(String url, Notificacion notificacion) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    @Override
    public void Get(String url) {
        AsyncHttpGet get = new AsyncHttpGet(url);
        AsyncHttpClient.getDefaultInstance().executeJSONArray(get, new AsyncHttpClient.JSONArrayCallback() {
            @Override
            public void onCompleted(Exception e, AsyncHttpResponse source, JSONArray result) {
                try{
                    if(listener != null) {
                        listener.onDataLoaded(result);
                    }
                }catch (Exception ex){ ex.printStackTrace(); }
            }
        });
    }

    @Override
    public void Get(String url, Notificacion notificacion) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }
}
