    package com.transportar.demo.transportar.Repository;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.transportar.demo.transportar.Entities.PuntoReferencia;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;

public class RepoPuntoReferencia extends SQLiteOpenHelper implements IRepository<PuntoReferencia> {
    Activity _Activity;
    public RepoPuntoReferencia(Context context) {
        super(context, "MoviluDB", null, 2);
    }

    public RepoPuntoReferencia(Activity activity) {
        super(activity.getApplicationContext(), "MoviluDB", null, 2);
        _Activity = activity;
    }

    public void Open() {
        this.getWritableDatabase();
    }

    public void Close() {
        this.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE PuntoReferencia(" + _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "nombre text,numero text, estado text, agregado text); ";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS PuntoReferencia";
        db.execSQL(query);

        onCreate(db);
    }


    @Override
    public boolean Add(PuntoReferencia puntoReferencia) {
        try {

            Open();
            ContentValues record = new ContentValues();
            record.put("nombre", puntoReferencia.getNombre());
            record.put("numero", puntoReferencia.getNumero());
            record.put("estado", puntoReferencia.getEstado());
            record.put("agregado", 1);
            this.getWritableDatabase().insert("PuntoReferencia", null, record);
            Close();
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public void Update(PuntoReferencia puntoReferencia) {
        try {

            Open();
            ContentValues record = new ContentValues();
            record.put("nombre", puntoReferencia.getNombre());
            record.put("numero", puntoReferencia.getNumero());
            record.put("estado", puntoReferencia.getEstado());
            SQLiteDatabase db = getWritableDatabase();
            //db.update("PuntoReferencia",record,"_ID=?", new String[]{puntoReferencia.getId()});

            db.update("PuntoReferencia", record,_ID +" = '"+ puntoReferencia.getId() + "'", null);

            Close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void Remove(PuntoReferencia puntoReferencia) {
        try{

            Open();
            SQLiteDatabase db = getWritableDatabase();
            db.delete("PuntoReferencia", _ID + "'" + puntoReferencia.getId() + "'", null);
            Close();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void Remove() {
        try {
            Open();
            SQLiteDatabase db = getWritableDatabase();
            db.delete("PuntoReferencia", null, null);
            db.close();
            Close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ArrayList<PuntoReferencia> Search() {
        int colId, colNombre, colNumero,colEstado, colAgregado;
        ArrayList<PuntoReferencia> puntoReferencias = new ArrayList<PuntoReferencia>();
        try{

            Open();
            String columnas[] = { _ID, "nombre", "numero", "estado", "agregado" };
            Cursor c = this.getReadableDatabase().query("PuntoReferencia", columnas, null,null, null, null, null);

            colId = c.getColumnIndex(_ID);
            colNombre = c.getColumnIndex("nombre");
            colNumero = c.getColumnIndex("numero");
            colEstado = c.getColumnIndex("estado");
            colAgregado = c.getColumnIndex("agregado");

            if (c.getCount() > 0) {
                c.moveToFirst();

                do {

                    PuntoReferencia puntoReferencia = new PuntoReferencia();
                    puntoReferencia.setId(c.getString(colId));
                    puntoReferencia.setNombre(c.getString(colNombre));
                    puntoReferencia.setIconoInicial(c.getString(colNombre), _Activity);
                    puntoReferencia.setNumero(Integer.parseInt(c.getString(colNumero)));
                    puntoReferencia.setEstado(Integer.parseInt(c.getString(colEstado)));
                    puntoReferencia.setAgregado(Integer.parseInt(c.getString(colAgregado)));
                    puntoReferencias.add(puntoReferencia);

                } while (c.moveToNext());
            }

            Close();

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return puntoReferencias;
    }

    @Override
    public PuntoReferencia Find(PuntoReferencia puntoReferencia) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

}
