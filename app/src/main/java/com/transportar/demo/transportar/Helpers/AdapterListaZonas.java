package com.transportar.demo.transportar.Helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.transportar.demo.transportar.R;

import java.util.ArrayList;

/**
 * Created by thiago on 28/03/2015.
 */
public class AdapterListaZonas extends BaseAdapter {
    private Activity activity;
    private ArrayList<LRutasSalida> lRutasSalidas = new ArrayList();

    public AdapterListaZonas(Activity activity, ArrayList<LRutasSalida> lista) {
        this.activity = activity;
        this.lRutasSalidas = lista;

    }

    @Override
    public int getCount() {
        return lRutasSalidas.size();
    }

    @Override
    public Object getItem(int position) {
        return lRutasSalidas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lRutasSalidas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vista = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) this.activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vista = inf.inflate(R.layout.item_zonas, null);
        }

        LRutasSalida lRutasSalida = lRutasSalidas.get(position);

        LinearLayout ic_zonas = (LinearLayout) vista.findViewById(R.id.iconZona);
        ic_zonas.setBackgroundDrawable(lRutasSalida.getIcono());

        TextView titulo = (TextView) vista.findViewById(R.id.txtNombreZona);
        titulo.setText(lRutasSalida.getTitulo());

        TextView barrios = (TextView) vista.findViewById(R.id.txtBarrios);
        barrios.setText(lRutasSalida.getBarrios());

        TextView hora = (TextView) vista.findViewById(R.id.txtHora);
        hora.setText(lRutasSalida.getHora());

        return vista;
    }
}
