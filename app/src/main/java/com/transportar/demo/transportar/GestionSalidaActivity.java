package com.transportar.demo.transportar;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.transportar.demo.transportar.Helpers.AdapterListaZonas;
import com.transportar.demo.transportar.Helpers.LRutasSalida;

import java.util.ArrayList;


public class GestionSalidaActivity extends Activity {

    Context contexto;
    Resources res;
    ActionBar barra;
    ListView listaRutas;
    AdapterListaZonas adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_salida);
        init();
        CargarDatos();
        Eventos();
    }

    private void init(){
        contexto=getApplicationContext();
        res=getResources();
        barra=getActionBar();
        barra.setBackgroundDrawable(res.getDrawable(R.color.rojo_oscuro));
        listaRutas=(ListView)findViewById(R.id.listViewZonas);
    }

    private void Eventos(){
        listaRutas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(contexto,ReservasActivity.class);
                startActivity(intent);
            }
        });

        listaRutas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                view.setActivated(true);
                view.setSelected(true);
                LRutasSalida rutasSalida=(LRutasSalida) adapter.getItem(position);
                rutasSalida.setTipo(2);
                rutasSalida.setIcono(res.getDrawable(R.drawable.fab_boton_azul));
                listaRutas.setAdapter(adapter);
                //Toast.makeText(contexto,"Holap",Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void CargarDatos(){
        ArrayList<LRutasSalida> lRutasSalidas = new ArrayList();
        LRutasSalida rutasSalida1 = new LRutasSalida(1,"Zona-A","blas de lezo, socorro, junin, manga,blas de lezo, socorro, junin, manga,...","7:00 am",0,0,this);
        LRutasSalida rutasSalida2 = new LRutasSalida(2,"Zona-B","blas de lezo, socorro, junin, manga,blas de lezo, socorro, junin, manga,...","9:00 am",0,0,this);
        LRutasSalida rutasSalida3 = new LRutasSalida(3,"Zona-C","blas de lezo, socorro, junin, manga,blas de lezo, socorro, junin, manga,...","11:00 am",0,0,this);
        LRutasSalida rutasSalida4 = new LRutasSalida(4,"Zona-D","blas de lezo, socorro, junin, manga,blas de lezo, socorro, junin, manga,...","1:00 pm",0,0,this);
        lRutasSalidas.add(rutasSalida1);
        lRutasSalidas.add(rutasSalida2);
        lRutasSalidas.add(rutasSalida3);
        lRutasSalidas.add(rutasSalida4);

        adapter=new AdapterListaZonas(this,lRutasSalidas);
        listaRutas.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gestion_salida, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
