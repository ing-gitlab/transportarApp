package com.transportar.demo.transportar.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.transportar.demo.transportar.Entities.Conductor;
import com.transportar.demo.transportar.HttpHelper.HttpConductor;
import com.transportar.demo.transportar.R;
import com.transportar.demo.transportar.Util.Validation;
import com.transportar.demo.transportar.Session.UserSessionManager;

import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class RegistroActivity extends Activity {

    private EditText txtnombres;
    private EditText txtapellidos;
    private EditText txttelefono;
    private EditText txtCorreo;
    private EditText txtContrasena;
    private EditText txtConfirmarContrasena;

    private ImageButton btnRegistrar;
    private String serverUrl;

    public ProgressDialog barProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Init();
        Events();
    }

    private void Init() {
        txtnombres = (EditText) findViewById(R.id.txtNombres);
        txtapellidos = (EditText) findViewById(R.id.txtApellidos);
        txttelefono = (EditText) findViewById(R.id.txtTelefono);
        txtCorreo = (EditText) findViewById(R.id.txtCorreo);
        txtContrasena = (EditText) findViewById(R.id.txtContrasena);
        txtConfirmarContrasena = (EditText) findViewById(R.id.txtConfirmarContrasena);

        btnRegistrar = (ImageButton) findViewById(R.id.btnRegistro);
        serverUrl = getApplicationContext().getResources().getString(R.string.ServerUrl);
    }

    private void Events() {
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validate()) {
                    JSONObject data = ValidateData();
                    try {
                        if (data.getBoolean("isValid")) {
                            barProgressDialog = new ProgressDialog(RegistroActivity.this);
                            barProgressDialog = ProgressDialog.show(RegistroActivity.this,"Transportar","Accediendo...");

                            Conductor conductor = Fill();
                            String url = serverUrl + getApplicationContext().getResources().getString(R.string.registrar);
                            HttpConductor http = new HttpConductor();
                            http.Post(url, conductor);
                            http.setObjectListener(new HttpConductor.HttpListener() {
                                @Override
                                public void onDataLoaded(JSONArray data) {}

                                @Override
                                public void onDataLoaded(JSONObject data) {
                                    try {
                                        int statusCode = data.getInt("statusCode");
                                        if (statusCode == 200) {
                                            final String token = data.getString("token");
                                            Conductor user = FormattedUser(data.getJSONObject("conductor"));
                                            CreateSession(user, token);
                                            IniciarActividadPrincipal();
                                        } else {
                                            final String msg = data.getString("message");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(RegistroActivity.this, msg, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                        barProgressDialog.dismiss();
                                    }catch(Exception ex){
                                        barProgressDialog.dismiss();
                                        ex.printStackTrace();
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(RegistroActivity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception ex){
                        Toast.makeText(RegistroActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegistroActivity.this, "Todos los campos son requeridos", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private Conductor Fill(){
        UserSessionManager session = new UserSessionManager(getApplicationContext());
        final String device = session.getStringPreference("deviceId");
        return new Conductor(){{
            setDeviceId(device);
            setNombre(txtnombres.getText().toString());
            setApellido(txtapellidos.getText().toString());
            setTelefono(txttelefono.getText().toString());
            setCorreo(txtCorreo.getText().toString());
            setContrasena(txtContrasena.getText().toString());
            setConfirmarContrasena(txtConfirmarContrasena.getText().toString());
        }};
    }

    public void IniciarActividadPrincipal(){
        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private boolean Validate() {

        if (txtnombres.getText().toString().equals("") &&
                txtapellidos.getText().toString().equals("") &&
                txttelefono.getText().toString().equals("") &&
                txtCorreo.getText().toString().equals("") &&
                txtContrasena.getText().toString().equals("")) {

            return false;
        }

        return true;
    }

    private JSONObject ValidateData() {
        boolean isValid = true;
        JSONObject jsonObject = new JSONObject();
        try{

            if (!Validation.Validar(txtCorreo.getText().toString(), 1)) {
                jsonObject.put("message", "Correo invalido");
            }

            if (!Validation.Validar(txttelefono.getText().toString(), 2)) {
                jsonObject.put("message", "Telefono invalido");
            }

            if (!txtContrasena.getText().toString().toLowerCase().equals(txtConfirmarContrasena.getText().toString().toLowerCase())) {
                jsonObject.put("message", "Las contraseñas no coinciden");
            }

            jsonObject.put("isValid", isValid);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return jsonObject;
    }

    private Conductor FormattedUser(final JSONObject user) {
        try{
            return new Conductor(){{
                setId(user.getString("_id"));
                setNombre(user.getString("nombres"));
                setApellido(user.getString("apellidos"));
                setCorreo(user.getString("correo"));
                setContrasena(user.getString("password"));
                setTelefono(user.getString("telefono"));
                setEstado(user.getString("estado"));
                setDeviceId(user.getString("device"));
            }};
        }catch (Exception ex){ return null;}
    }

    public void CreateSession(Conductor conductor, String token) {
        UserSessionManager session = new UserSessionManager(getApplicationContext());
        session.createUserLoginSession(conductor, token);
    }

}
