package com.transportar.demo.transportar.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.transportar.demo.transportar.Entities.Conductor;
import com.transportar.demo.transportar.Entities.Localizacion;
import com.transportar.demo.transportar.Entities.Pasajero;
import com.transportar.demo.transportar.Activities.MainActivity;
import com.transportar.demo.transportar.Entities.Servicio;
import com.transportar.demo.transportar.R;
import com.transportar.demo.transportar.Session.UserSessionManager;


import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

public class MsgService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try{
            if (remoteMessage.getData().size() > 0) {
                JSONObject jsonServicio = new JSONObject(remoteMessage.getData());
                Servicio servicio = Fill(jsonServicio);
                if(servicio != null) {
                    SendBroadcastMessage(servicio);
                    CreateNotification(servicio.getPasajero());
                }
            }

        /*
        //String message = "";
        if (remoteMessage.getNotification() != null) {
            message = remoteMessage.getNotification().getBody();
        }
        String from = remoteMessage.getFrom();
        System.out.println(from);
        */

        }catch (Exception ex){ System.out.println("Error: " + ex.toString()); }
    }

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return new String[2];

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }

    private Servicio Fill(final JSONObject servicio){
        if(servicio.length() == 0) return null;
        try {
            JSONObject jsonPasajero = new JSONObject(servicio.getString("pasajero").toString());
            final JSONObject pasajero = jsonPasajero;

            final JSONObject jsonServicioLocation = new JSONObject(servicio.getString("location"));
            final JSONObject jsonPasajeroLocation = new JSONObject(pasajero.getString("location"));

            final String [] sLocation = toStringArray(jsonServicioLocation.getJSONArray("coordinates"));
            final String [] pLocation = toStringArray(jsonPasajeroLocation.getJSONArray("coordinates"));

            return new Servicio(){{
                setId(servicio.getString("_id"));
                setConductorId(servicio.getString("conductor"));
                setEstado(servicio.getString("estado"));
                setFecha(servicio.getString("fecha"));
                setLocation(new Localizacion(){{
                    setCoordenadas(sLocation);
//                    setType(jsonServicioLocation.getString("type"));
                }});
                setPasajero(new Pasajero(){{
                    setId(pasajero.getString("_id"));
                    setNombres(pasajero.getString("nombres"));
                    setApellidos(pasajero.getString("apellidos"));
                    setReferencia(pasajero.getString("referencia"));
                    setTelefono(pasajero.getString("telefono"));
                    setLocation(new Localizacion(){{
                        setCoordenadas(pLocation);
//                        setType(jsonPasajeroLocation.getString("type"));
                    }});
                }});
            }};
        }catch (Exception ex){ return null; }
    }

    private void SendBroadcastMessage(final Servicio servicio){
        try{

            Intent intent = new Intent(MainActivity.RECEIVE_JSON);
            intent.putExtra("servicios", new ArrayList<Servicio>(){{ add(servicio); }});

            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }catch (Exception ex) { }
    }

    private void CreateNotification(Pasajero pasajero) {
        try{
            Intent intent = new Intent(this , MainActivity.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this)
                    .setSmallIcon(R.mipmap.ic_user_maker)
                    .setContentTitle("Transportar app")
                    .setContentText(pasajero.getNombres() + " " + pasajero.getApellidos() +  " asignado. ")
                    .setAutoCancel( true )
                    .setSound(notificationSoundURI)
                    .setContentIntent(resultIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, mNotificationBuilder.build());
        }catch(Exception ex) {}
    }
}
