package com.transportar.demo.transportar.Entities;

/**
 * Created by hjimenez on 06/15/2017.
 */

public class Servicio {
    String Id;
    String ConductorId;
    String Estado;
    String Fecha;
    Localizacion Location;
    Pasajero Pasajero;

    public Servicio(){}

    public Servicio(String id, String conductorId, String estado, String fecha, Localizacion location, com.transportar.demo.transportar.Entities.Pasajero pasajero) {
        Id = id;
        ConductorId = conductorId;
        Estado = estado;
        Fecha = fecha;
        Location = location;
        Pasajero = pasajero;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getConductorId() {
        return ConductorId;
    }

    public void setConductorId(String conductorId) {
        ConductorId = conductorId;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public Localizacion getLocation() {
        return Location;
    }

    public void setLocation(Localizacion location) {
        Location = location;
    }

    public com.transportar.demo.transportar.Entities.Pasajero getPasajero() {
        return Pasajero;
    }

    public void setPasajero(com.transportar.demo.transportar.Entities.Pasajero pasajero) {
        Pasajero = pasajero;
    }
}
