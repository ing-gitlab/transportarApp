package com.transportar.demo.transportar.HttpHelper;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IHttpHelper <T> {
    interface HttpListener {
        void onDataLoaded(JSONArray data);
        void onDataLoaded(JSONObject data);
    }

    void Post(String url, T entidad);
    void Get(String url);
    void Get(String url, T entidad);
}
