package com.transportar.demo.transportar.Helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.transportar.demo.transportar.Entities.PuntoReferencia;
import com.transportar.demo.transportar.R;

import java.util.ArrayList;


public class AdapterListaPuntosReferencia extends BaseAdapter {
    private Activity activity;
    private ArrayList<PuntoReferencia> puntosReferenciaArrayList;


    public AdapterListaPuntosReferencia(Activity activity, ArrayList<PuntoReferencia> lista) {
        this.activity = activity;
        this.puntosReferenciaArrayList = lista;

    }

    @Override
    public int getCount() {
        return puntosReferenciaArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return puntosReferenciaArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;//puntosReferenciaArrayList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vista = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vista = inf.inflate(R.layout.item_lista_puntos_referencias_numero, null);
        }

        PuntoReferencia puntoReferencia = puntosReferenciaArrayList.get(position);

        LinearLayout ic_estudiante = (LinearLayout) vista.findViewById(R.id.id_ic_punto_referencia);
        ic_estudiante.setBackgroundDrawable(puntoReferencia.getIcono());

        TextView nombre = (TextView) vista.findViewById(R.id.txtNombrePuntoReferencia);
        nombre.setText(puntoReferencia.getNombre());

        TextView inicial = (TextView) vista.findViewById(R.id.txtInicial);

        inicial.setText(puntoReferencia.getInicial(puntoReferencia.getNombre()));

        TextView numero = (TextView) vista.findViewById(R.id.txtNumero);
        numero.setText("" + puntoReferencia.getNumero());

        return vista;
    }
}
