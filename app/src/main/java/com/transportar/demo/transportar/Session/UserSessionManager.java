package com.transportar.demo.transportar.Session;

import java.util.HashMap;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.transportar.demo.transportar.Entities.Conductor;

public class UserSessionManager {
    SharedPreferences sharedPreferences;
    Editor editor;
    int PRIVATE_MODE = 0;

    private static final String PREFERENCE_NAME = "MyPreferences";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    public static final String KEY_ID = "id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_NAME = "name";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DEVICE = "device";
    public static final String KEY_TOKEN = "token";

    public UserSessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createUserLoginSession(Conductor conductor, String token) {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_ID, conductor.getId());
        editor.putString(KEY_USERNAME, conductor.getCorreo());
        editor.putString(KEY_PASSWORD, conductor.getContrasena());
        editor.putString(KEY_NAME, conductor.getNombre());
        editor.putString(KEY_LASTNAME, conductor.getApellido());
        editor.putString(KEY_PHONE, conductor.getTelefono());
        editor.putString(KEY_STATUS, conductor.getEstado());
        editor.putString(KEY_DEVICE, conductor.getDeviceId());
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public Conductor getUserDetails(){
        return new Conductor(){{
            setId(sharedPreferences.getString(KEY_ID, null));
            setNombre(sharedPreferences.getString(KEY_NAME, null));
            setApellido(sharedPreferences.getString(KEY_LASTNAME, null));
            setCorreo(sharedPreferences.getString(KEY_USERNAME, null));
            setContrasena(sharedPreferences.getString(KEY_PASSWORD, null));
            setTelefono(sharedPreferences.getString(KEY_PHONE, null));
            setEstado(sharedPreferences.getString(KEY_STATUS, null));
            setDeviceId(sharedPreferences.getString(KEY_DEVICE, null));
        }};
    }

    public boolean checkLogin() {
        if (this.isUserLoggedIn()) return true;
        return false;
    }

    public void logoutUser() {
        editor.putBoolean(IS_USER_LOGIN, false);
        editor.commit();
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean(IS_USER_LOGIN, false);
    }

    public void addPreference(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringPreference(String key) {
        return sharedPreferences.getString(key, "none");
    }

}
