package com.transportar.demo.transportar.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
    private String campo;
    private int tipo;

    public Validation() {
        this.campo = campo;
        this.tipo = tipo;
    }

    public static boolean Validar(String campo, int tipo) {

		/*
		 * 1:correo
		 * 2:celular
		 * 3:campos vacios
		 * *
		 */

        boolean valida = false;
        switch (tipo) {
            case 1:
                Pattern pat = Pattern
                        .compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                Matcher mat = pat.matcher(campo);
                if (mat.find()) {
                    valida = true;
                } else {
                    valida = false;
                }
                break;

            case 2:
                Pattern pat2 = Pattern.compile("^[0-9]{3}? ?[0-9]{7}$");
                Matcher mat2 = pat2.matcher(campo);
                if (mat2.find()) {
                    valida = true;
                } else {
                    valida = false;
                }

                break;

            case 3:
                if(!campo.equals("")){
                    System.out.println("Campo Lleno");
                    valida = true;
                }else{
                    System.out.println("Campo Vacio");
                    valida = false;
                }
                break;
        }
        return valida;

    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
