package com.transportar.demo.transportar.Entities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.transportar.demo.transportar.R;


public class ItemNotificacion implements Parcelable {
    private String Id;
    private String Titulo;
    private Drawable Icono;
    private String Descripcion;
    private String Fecha;
    private String Avatar;
    private int Estado;
    private String[] Coordenadas;

    public ItemNotificacion(){}

    protected ItemNotificacion(Parcel in) {
        Id = in.readString();
        Titulo = in.readString();
        Descripcion = in.readString();
        Fecha = in.readString();
        Avatar = in.readString();
        Estado = in.readInt();
        Coordenadas = in.createStringArray();
    }

    public static final Creator<ItemNotificacion> CREATOR = new Creator<ItemNotificacion>() {
        @Override
        public ItemNotificacion createFromParcel(Parcel in) {
            return new ItemNotificacion(in);
        }

        @Override
        public ItemNotificacion[] newArray(int size) {
            return new ItemNotificacion[size];
        }
    };

    public void setAvatar(int tipo, String avatar, Activity activity) {
        this.Avatar = avatar;
        switch (tipo) {
            case 1:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_azul));

                break;
            case 2:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_amarillo));

                break;
            case 3:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_verde));

                break;
            case 4:
                setIcono(activity.getResources().getDrawable(R.drawable.fab_boton_rojo));

                break;
        }
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public Drawable getIcono() {
        return Icono;
    }

    public void setIcono(Drawable icono) {
        Icono = icono;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int estado) {
        Estado = estado;
    }

    public String[] getCoordenadas() {
        return Coordenadas;
    }

    public void setCoordenadas(String[] coordenadas) {
        Coordenadas = coordenadas;
    }

    public String getAvatar() {
        return Avatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(Titulo);
        dest.writeString(Descripcion);
        dest.writeString(Fecha);
        dest.writeString(Avatar);
        dest.writeInt(Estado);
        dest.writeStringArray(Coordenadas);
    }
}
