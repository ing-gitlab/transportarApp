package com.transportar.demo.transportar.Entities;

/**
 * Created by hjimenez on 06/15/2017.
 */

public class Localizacion {
    String[] Coordenadas;
    String Type;

    public Localizacion(){}

    public Localizacion(String[] coordenadas, String type) {
        Coordenadas = coordenadas;
        Type = type;
    }

    public String[] getCoordenadas() {
        return Coordenadas;
    }

    public void setCoordenadas(String[] coordenadas) {
        Coordenadas = coordenadas;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
