package com.transportar.demo.transportar.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.transportar.demo.transportar.Entities.ItemNotificacion;
import com.transportar.demo.transportar.R;


public class DetalleNotificacionActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_notificacion);
        SetUpMap();
        init();
    }

    private void SetUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void init(){
        TextView txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        TextView txtDetalle = (TextView) findViewById(R.id.txtDetalleNotificacion);
        TextView txtFecha = (TextView) findViewById(R.id.txtFechaNotificacion);

        ItemNotificacion itemNotificacion = getIntent().getExtras().getParcelable("Notificacion");
        txtTitulo.setText(itemNotificacion.getTitulo());
        txtDetalle.setText(itemNotificacion.getDescripcion());
        txtFecha.setText(itemNotificacion.getFecha());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        ItemNotificacion itemNotificacion = getIntent().getExtras().getParcelable("Notificacion");
        String[] coordinates = itemNotificacion.getCoordenadas();

        double latitude = Double.parseDouble(coordinates[0]);
        double longitude = Double.parseDouble(coordinates[1]);
        LatLng location = new LatLng(latitude, longitude);

        mMap.addMarker(new MarkerOptions().position(location).title("Posición del pasajero"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18));
    }
}
