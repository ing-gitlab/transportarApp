package com.transportar.demo.transportar.HttpHelper;

import android.app.Activity;
import android.app.ProgressDialog;

import com.transportar.demo.transportar.Entities.PuntoReferencia;
import com.transportar.demo.transportar.R;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HttpPuntoReferencia implements IHttpHelper<PuntoReferencia> {

    /*
    @Override
    public void Get(Activity activity) {

        final RegistroPuntosReferenciaActivity ra = ((RegistroPuntosReferenciaActivity)activity);
        final ArrayList<PuntoReferencia> puntoReferencias = new ArrayList<PuntoReferencia>();

        ra.barProgressDialog = new ProgressDialog(ra);
        ra.barProgressDialog = ProgressDialog.show(ra,"Movilú","Cargando puntos de referencias...");


        String serverUrl = ra.getApplicationContext().getResources().getString(R.string.ServerUrl);
        String apiUrl = ra.getApplicationContext().getResources().getString(R.string.getUniversities);
        String url = serverUrl + apiUrl;

        AsyncHttpClient.getDefaultInstance().executeJSONArray(new AsyncHttpGet(url), new AsyncHttpClient.JSONArrayCallback() {
            @Override
            public void onCompleted(Exception arg0,AsyncHttpResponse arg1, final JSONArray jsonArray) {
                try {

                    for (int i = 0; i < jsonArray.length(); i++) {

                        String id = jsonArray.getJSONObject(i).getString("_id");
                        String name = jsonArray.getJSONObject(i).getString("nombre");


                        ra.barProgressDialog.dismiss();
                    }

                } catch (Exception e) {
                    ra.barProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        });
    }*/

    @Override
    public void Post(String url, PuntoReferencia entidad) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    @Override
    public void Get(String url, PuntoReferencia entidad) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    @Override
    public void Get(String url) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }
}
