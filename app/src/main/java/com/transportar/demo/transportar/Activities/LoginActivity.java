        package com.transportar.demo.transportar.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.transportar.demo.transportar.Entities.Conductor;
import com.transportar.demo.transportar.HttpHelper.HttpConductor;
import com.transportar.demo.transportar.R;
import com.transportar.demo.transportar.Session.UserSessionManager;

import android.widget.Toast;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.transportar.demo.transportar.Util.Validation;
import com.transportar.demo.transportar.Util.InternetConexion;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginActivity extends Activity implements Serializable{

    private TextView txtCorreo;
    private TextView txtContrasena;
    private Button btnIniciarSesion;
    private Button btnRegistrar;
    public ProgressDialog barProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        CheckLogin();
        init();
        Eventos();
    }

    private void init(){
        ActionBar barra = getActionBar();
        barra.setBackgroundDrawable(getResources().getDrawable(R.color.azul_oscuro));
        txtCorreo=(TextView)findViewById(R.id.txtCorreo);
        txtContrasena=(TextView)findViewById(R.id.txtContrasena);

        btnIniciarSesion=(Button)findViewById(R.id.btnIniciarSesion);
        btnRegistrar=(Button)findViewById(R.id.btnRegistrar);
    }

    private void CheckLogin() {
        UserSessionManager session = new UserSessionManager(LoginActivity.this);
        if(session.checkLogin()) {
            Conductor userLogged = session.getUserDetails();
            IniciarActividadPrincipal();
        }
    }

    private void DisplayToast(final String msg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Eventos(){
        //Iniciar sesion
        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String correo = txtCorreo.getText().toString();
                final String contrasena = txtContrasena.getText().toString();

                if (Validation.Validar(correo, 1)) {

                    barProgressDialog = new ProgressDialog(LoginActivity.this);
                    barProgressDialog = ProgressDialog.show(LoginActivity.this,"Transportar","Accediendo...");

                    Conductor conductor = new Conductor(){{
                        setCorreo(correo);
                        setContrasena(contrasena);
                    }};

                    String url = getResources().getString(R.string.ServerUrl)+ "/login";
                    final String token = getToken();

                    HttpConductor http = new HttpConductor();
                    http.PostLogin(url, token, conductor);
                    http.setObjectListener(new HttpConductor.HttpListener(){

                        @Override
                        public void onDataLoaded(JSONArray data) {

                        }

                        @Override
                        public void onDataLoaded(JSONObject data) {
                            try {

                                int statusCode = data.getInt("statusCode");
                                if (statusCode == 200) {
                                    Conductor user = FormattedUser(data.getJSONObject("user"));
                                    if(user == null) DisplayToast("Error mapeando los datos del usuario");
                                    CreateSession(user, token);
                                    IniciarActividadPrincipal();
                                } else {
                                    final String msg = data.getString("message");
                                    DisplayToast(msg);
                                }
                                barProgressDialog.dismiss();
                            }catch(Exception ex){
                                barProgressDialog.dismiss();
                                ex.printStackTrace();
                            }
                        }
                    });

                    Timer timer = new Timer();
                    TimerTask timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            if(!InternetConexion.verificaConexion(getApplicationContext())){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(),"Por favor verifica tu conexión a internet", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    };
                    timer.schedule(timerTask, 15000);

                } else {
                    Toast.makeText(LoginActivity.this,"El correo "+ correo + " no es válido.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Registrarse
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), RegistroActivity.class);
                startActivity(i);
            }
        });

        //Cambiar contraseña
        findViewById(R.id.txtAyuda).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Recupera tu contraseña en www.movilu.com/login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Conductor FormattedUser(final JSONObject user) {
        try{
            return new Conductor(){{
                setId(user.getString("_id"));
                setNombre(user.getString("nombres"));
                setApellido(user.getString("apellidos"));
                setCorreo(user.getString("correo"));
                setContrasena(user.getString("password"));
                setTelefono(user.getString("telefono"));
                setEstado(user.getString("estado"));
                setDeviceId(user.getString("device"));
            }};
        }catch (Exception ex){ return null;}
    }

    private void CreateSession(Conductor conductor, String token) {
        UserSessionManager session = new UserSessionManager(getApplicationContext());
        session.createUserLoginSession(conductor, token);
    }

    private String getToken(){
        UserSessionManager session = new UserSessionManager(LoginActivity.this);
        return session.getStringPreference("token");
    }

    public void IniciarActividadPrincipal(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
