/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.transportar.demo.transportar.Activities;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.transportar.demo.transportar.Entities.Conductor;
import com.transportar.demo.transportar.Entities.Device;
import com.transportar.demo.transportar.Entities.Localizacion;
import com.transportar.demo.transportar.Entities.Pasajero;
import com.transportar.demo.transportar.Entities.Servicio;
import com.transportar.demo.transportar.GestionSalidaActivity;
import com.transportar.demo.transportar.Helpers.AdapterListaNotificaciones;
import com.transportar.demo.transportar.Entities.ItemNotificacion;
import com.transportar.demo.transportar.HttpHelper.HttpDevice;
import com.transportar.demo.transportar.HttpHelper.HttpConductor;
import com.transportar.demo.transportar.HttpHelper.HttpNotificacion;
import com.transportar.demo.transportar.R;
import com.transportar.demo.transportar.Session.UserSessionManager;
import com.transportar.demo.transportar.Util.GPSTracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends FragmentActivity implements Serializable{
    public static final String RECEIVE_JSON = "RECEIVE_JSON";

    ArrayList<ItemNotificacion> Notifications = new ArrayList<>();
    AdapterListaNotificaciones Adapter;
    ListView ListaNotificaciones;
    LocalBroadcastManager bManager;

//    ImageButton btnMostrarSalida;
    TextView txtNombreUsuario;
    TextView txtDireccion;
//    TextView btnEstado;

    private final static int INTERVAL = 15000; //15 segundos

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CheckLogin();
        GetNotifications();
        InitializeBroadcast();
        InitializeComponents();
        Events();
        startRepeatingTask();
    }


    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable(){
        @Override
        public void run() {
            SendLocationToServer();
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    void startRepeatingTask()
    {
        mHandlerTask.run();
    }

    void stopRepeatingTask()
    {
        mHandler.removeCallbacks(mHandlerTask);
    }

    public void onDestroy(){
        super.onDestroy();
        stopRepeatingTask();
        bManager.unregisterReceiver(bReceiver);
    }

    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if(intent.getAction().equals(RECEIVE_JSON)) {
                //String serverUrl = getApplicationContext().getResources().getString(R.string.ServerUrl);
                //String url = serverUrl + getApplicationContext().getResources().getString(R.string.coordenadas);

                ArrayList<Servicio> servicios = (ArrayList<Servicio>) intent.getSerializableExtra("servicios");
                RefreshMessages(servicios);
            }
        }
    };

    private Boolean NotificationExists(Servicio servicio){
        for (Iterator<ItemNotificacion> i = Notifications.iterator(); i.hasNext();) {
            ItemNotificacion item = i.next();
            if(item.getId().equals(servicio.getId())) return true;
        }
        return false;
    }

    private void FillNotification(ArrayList<Servicio> servicios){
        for (Iterator<Servicio> item = servicios.iterator(); item.hasNext();) {
            final Servicio servicio = item.next();

            final Pasajero pasajero = servicio.getPasajero();
            final String avatar = (pasajero.getNombres().charAt(0) + "" + pasajero.getApellidos().charAt(0)).toUpperCase();
            final String title = pasajero.getNombres() + " " + pasajero.getApellidos();
            final String description =  pasajero.getReferencia() + ", " + pasajero.getTelefono();
            final String time = servicio.getFecha();
            final String[] coordinates = pasajero.getLocation().getCoordenadas();

            Notifications.add(new ItemNotificacion(){{
                setId(servicio.getId());
                setAvatar(1, avatar, MainActivity.this);
                setTitulo(title);
                setDescripcion(description);
                setFecha(time);
                setCoordenadas(coordinates);
            }});
        }

        RefreshList();
    }

    public void RefreshMessages(ArrayList<Servicio> servicios){
        final Servicio servicio = servicios.get(0);
        if(!NotificationExists(servicio)) FillNotification(servicios);
    }

    private void RefreshList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Adapter.notifyDataSetChanged();
                ListaNotificaciones.invalidateViews();
                ListaNotificaciones.refreshDrawableState();
            }
        });
    }

    private void CheckLogin() {
        UserSessionManager session = new UserSessionManager(MainActivity.this);
        if(!session.checkLogin()) Logout();
    }

    private String getToken(){
        UserSessionManager session = new UserSessionManager(MainActivity.this);
        return session.getStringPreference("token");
    }

    private void GetNotifications() {
        UserSessionManager session = new UserSessionManager(MainActivity.this);
        Conductor conductor = session.getUserDetails();

        final String avatar = (conductor.getNombre().charAt(0) + "" + conductor.getApellido().charAt(0)).toUpperCase();
        Notifications.add(new ItemNotificacion(){{
            setId("1");
            setAvatar(1, avatar, MainActivity.this);
            setTitulo("Listado de notificaciones");
            setDescripcion("En este espacio encontraras los pasajeros asignados a tu ruta");
            setFecha("7:00 am");
            setEstado(0);
        }});

        String url = getString(R.string.ServerUrl) + getString(R.string.getServicios) + conductor.getId();
        HttpNotificacion http = new HttpNotificacion();
        http.Get(url);
        http.setObjectListener(new HttpNotificacion.HttpListener() {
            @Override
            public void onDataLoaded(JSONArray data) {
                try {
                    if (data.length() > 0) {
                        ArrayList<Servicio> servicios = new ArrayList<Servicio>();
                        for(int i = 0; i < data.length(); i++){
                            JSONObject jsonObject = data.getJSONObject(i);
                            Servicio servicio = Fill(jsonObject);
                            servicios.add(servicio);
                        }

                        FillNotification(servicios);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onDataLoaded(JSONObject data) { }
        });
    }

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return new String[2];

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }

    private Servicio Fill(final JSONObject servicio){
        if(servicio.length() == 0) return null;
        try {
            final JSONObject jsonPasajero = new JSONObject(servicio.getString("pasajero").toString());
            final JSONObject pasajero = jsonPasajero;

            final JSONObject jsonServicioLocation = new JSONObject(servicio.getString("location"));
            final JSONObject jsonPasajeroLocation = new JSONObject(pasajero.getString("location"));

            final String [] sLocation = toStringArray(jsonServicioLocation.getJSONArray("coordinates"));
            final String [] pLocation = toStringArray(jsonPasajeroLocation.getJSONArray("coordinates"));

            System.out.println("servicio.location: " + sLocation[0] + " , " + sLocation[1]);
            System.out.println("pasajero.location: " + pLocation[0] + " , " + pLocation[1]);

            return new Servicio(){{
                setId(servicio.getString("_id"));
                setConductorId(servicio.getString("conductor"));
                setEstado(servicio.getString("estado"));
                setFecha(servicio.getString("fecha"));
                setLocation(new Localizacion(){{
                    setCoordenadas(sLocation);
//                    setType(jsonServicioLocation.getString("type"));
                }});
                setPasajero(new Pasajero(){{
                    setId(pasajero.getString("_id"));
                    setNombres(pasajero.getString("nombres"));
                    setApellidos(pasajero.getString("apellidos"));
                    setReferencia(pasajero.getString("referencia"));
                    setTelefono(pasajero.getString("telefono"));
                    setLocation(new Localizacion(){{
                        setCoordenadas(pLocation);
//                        setType(jsonPasajeroLocation.getString("type"));
                    }});
                }});
            }};
        }catch (Exception ex){ return null; }
    }

    private void SendLocationToServer() {
        GPSTracker gps = new GPSTracker(getApplicationContext());
        final double latitude = gps.getLatitude();
        final double longitude= gps.getLongitude();

        String url = getString(R.string.ServerUrl) + getString(R.string.coordenadas);
        String token = getToken();
        HttpDevice http = new HttpDevice();
        http.Put(url, token, new Device(){{
            setLatitude(latitude);
            setLongitude(longitude);
        }});
        http.setObjectListener(new HttpConductor.HttpListener() {
            @Override
            public void onDataLoaded(JSONArray data) {}

            @Override
            public void onDataLoaded(JSONObject data) {
                try {
                    int statusCode = data.getInt("statusCode");
                    if (statusCode == 200) {
                        Toast.makeText(MainActivity.this, "Device updated", Toast.LENGTH_LONG).show();
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }

    private void InitializeBroadcast() {
        bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVE_JSON);
        bManager.registerReceiver(bReceiver, intentFilter);
    }

    private void InitializeComponents() {
        ActionBar barra = getActionBar();
        barra.setBackgroundDrawable(getResources().getDrawable(R.color.azul_claro));

        UserSessionManager session = new UserSessionManager(MainActivity.this);
        Conductor conductor = session.getUserDetails();

        Adapter = new AdapterListaNotificaciones(MainActivity.this, Notifications);
        ListaNotificaciones = (ListView) findViewById(R.id.listViewNotificacion);
        ListaNotificaciones.setAdapter(Adapter);

//        btnMostrarSalida = (ImageButton)findViewById(R.id.btnMostarSalida);
        txtNombreUsuario = (TextView) findViewById(R.id.txtNombreUsuario);
        txtDireccion = (TextView) findViewById(R.id.txtDireccion);
//        btnEstado = (Button) findViewById(R.id.btnEstado);

        txtNombreUsuario.setText(conductor.getNombre() + " " + conductor.getApellido());
        txtDireccion.setText(conductor.getCorreo());
//        btnEstado.setText(getIntent().getStringExtra("estado"));

    }

    private void Events(){
//        btnMostrarSalida.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(getApplicationContext(),GestionSalidaActivity.class);
//                startActivity(intent);
//            }
//        });

        ListaNotificaciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) return;
                GoDetails(position);

            }
        });
    }

    private void GoDetails(int position){
        Intent intent = new Intent(MainActivity.this, DetalleNotificacionActivity.class);
        ItemNotificacion item = Notifications.get(position);
        intent.putExtra("Notificacion", item);
        startActivity(intent);
    }

    private void Logout() {

        UserSessionManager session = new UserSessionManager(MainActivity.this);
        session.logoutUser();

        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            Logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
