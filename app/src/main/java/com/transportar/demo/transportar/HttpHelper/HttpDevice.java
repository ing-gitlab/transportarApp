package com.transportar.demo.transportar.HttpHelper;


import android.app.Activity;

import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpPost;
import com.koushikdutta.async.http.AsyncHttpPut;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.body.JSONObjectBody;
import com.transportar.demo.transportar.Entities.Device;

import org.json.JSONArray;
import org.json.JSONObject;

public class HttpDevice implements IHttpHelper<Device>{
    public HttpListener listener = null;
    public void setObjectListener(HttpListener listener) {
        this.listener = listener;
    }

    @Override
    public void Post(String url, Device device) {
        AsyncHttpPost post = new AsyncHttpPost(url);
        JSONObjectBody jsonBody = new JSONObjectBody(BuildUserJson(device));
        post.setBody(jsonBody);

        AsyncHttpClient.getDefaultInstance().executeJSONObject(post,
                new AsyncHttpClient.JSONObjectCallback() {

                    @Override
                    public void onCompleted(Exception arg0, AsyncHttpResponse arg1, JSONObject res) {
                        try{

                            if(listener != null) {
                                listener.onDataLoaded(res);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void Get(String url, Device entidad) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    @Override
    public void Get(String url) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    public void Put(String url, String token, Device device) {
        AsyncHttpPut put = new AsyncHttpPut(url);
        JSONObjectBody jsonBody = new JSONObjectBody(BuildJson(device));
        put.setHeader("Authorization", "Bearer " + token);
        put.setBody(jsonBody);

        AsyncHttpClient.getDefaultInstance().executeJSONObject(put,
                new AsyncHttpClient.JSONObjectCallback() {

                    @Override
                    public void onCompleted(Exception arg0, AsyncHttpResponse arg1, JSONObject res) {
                        try{

                            if(listener != null) {
                                listener.onDataLoaded(res);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
    }

    private JSONObject BuildJson(Device device){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(device.getLatitude());
            jsonArray.put(device.getLongitude());
            jsonObject.put("type", "Point");
            jsonObject.put("coordinates", jsonArray);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return jsonObject;
    }

    private JSONObject BuildUserJson(Device device){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject();
            jsonObject.put("registrationId", device.getRegistrationId());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return jsonObject;
    }
}
