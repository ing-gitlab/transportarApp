package com.transportar.demo.transportar.Entities;

/**
 * Created by hjimenez on 06/05/2017.
 */

public class Pasajero {
    String Nombres;
    String Apellidos;
    String Referencia;
    String Telefono;
    String ConductorId;
    String Correo;
    Localizacion Location;

    public Pasajero(){}

    public Pasajero(String nombres, String apellidos, String referencia, String telefono, String conductorId, String correo, Localizacion location) {
        Nombres = nombres;
        Apellidos = apellidos;
        Referencia = referencia;
        Telefono = telefono;
        ConductorId = conductorId;
        Correo = correo;
        Location = location;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getReferencia() {
        return Referencia;
    }

    public void setReferencia(String referencia) {
        Referencia = referencia;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getConductorId() {
        return ConductorId;
    }

    public void setConductorId(String conductorId) {
        ConductorId = conductorId;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public Localizacion getLocation() {
        return Location;
    }

    public void setLocation(Localizacion location) {
        Location = location;
    }
}
