package com.transportar.demo.transportar.Firebase;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.transportar.demo.transportar.Entities.Device;
import com.transportar.demo.transportar.HttpHelper.HttpDevice;
import com.transportar.demo.transportar.HttpHelper.HttpConductor;
import com.transportar.demo.transportar.R;
import com.transportar.demo.transportar.Session.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

public class InstanceIdService extends FirebaseInstanceIdService {
    UserSessionManager session;
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String serverUrl = getApplicationContext().getResources().getString(R.string.ServerUrl);
        String url = serverUrl + getApplicationContext().getResources().getString(R.string.registrationId);
        System.out.println("Refreshed token: " + refreshedToken);
        sendRegistrationToServer(url, refreshedToken);
    }

    private void sendRegistrationToServer(String url, final String token) {
        HttpDevice http = new HttpDevice();
        http.Post(url, new Device(){{
            setRegistrationId(token);
        }});
        http.setObjectListener(new HttpConductor.HttpListener() {
            @Override
            public void onDataLoaded(JSONArray data) {}

            @Override
            public void onDataLoaded(JSONObject data) {
                try {
                    int statusCode = data.getInt("statusCode");
                    if (statusCode == 200) {
                        String deviceId = data.getString("id");
                        session = new UserSessionManager(getApplicationContext());
                        session.addPreference("deviceId", deviceId);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }
}