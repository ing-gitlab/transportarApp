package com.transportar.demo.transportar.Repository;

import java.util.ArrayList;

public interface IRepository<T> {

    boolean Add(T object);
    void Update(T object);
    void Remove(T object);
    void Remove();
    ArrayList<T> Search();
    T Find(T object);
}
