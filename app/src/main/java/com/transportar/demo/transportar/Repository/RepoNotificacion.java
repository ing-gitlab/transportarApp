package com.transportar.demo.transportar.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.transportar.demo.transportar.Entities.Notificacion;
import java.util.ArrayList;

import static android.provider.BaseColumns._ID;

public class RepoNotificacion extends SQLiteOpenHelper implements IRepository<Notificacion>{
    public RepoNotificacion(Context context) {
        super(context, "Transportar", null, 1);
    }

    public void Open() {
        this.getWritableDatabase();
    }

    public void Close() {
        this.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE Notificacion(" + _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "fecha text,hora text, tipo integer, contenido text); ";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS Notificacion";
        db.execSQL(query);

        onCreate(db);
    }


    public boolean Add(Notificacion notificacion) {
        boolean result = false;
        try {

            if (!notificacion.getContenido().isEmpty()) {

                Open();
                ContentValues record = new ContentValues();
                record.put("fecha", notificacion.getFecha());
                record.put("hora", notificacion.getHora());
                record.put("tipo", notificacion.getTipo());
                record.put("contenido", notificacion.getContenido());
                SQLiteDatabase db = getWritableDatabase();
                db.insert("Notificacion", null, record);
                Close();
                result = true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        }

        return result;
    }

    @Override
    public void Update(Notificacion object) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

    public void Remove() {
        try {
            Open();
            SQLiteDatabase db = getWritableDatabase();
            db.delete("Notificacion", null, null);
            db.close();
            Close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void Remove(Notificacion Notificacion) {
        try{

            Open();
            SQLiteDatabase db = getWritableDatabase();
            db.delete("Notificacion", "ID=" + Notificacion.getIdNotificacion(), null);
            Close();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public ArrayList<Notificacion> Search() {
        int colId, colFecha,colHora, colTipo,colMensaje;
        ArrayList<Notificacion> Notificacions = new ArrayList<Notificacion>();
        try{

            Open();
            String columnas[] = { _ID, "fecha", "hora" , "tipo", "contenido" };
            Cursor c = this.getReadableDatabase().query("Notificacion", columnas, null,null, null, null, null);

            colId = c.getColumnIndex(_ID);
            colFecha = c.getColumnIndex("fecha");
            colHora = c.getColumnIndex("hora");
            colTipo = c.getColumnIndex("tipo");
            colMensaje = c.getColumnIndex("contenido");

            if (c.getCount() > 0) {
                c.moveToFirst();

                do {
                    Notificacion notificacion = new Notificacion();
                    notificacion.setID(c.getString(colId));
                    notificacion.setFecha(c.getString(colFecha));
                    notificacion.setHora(c.getString(colHora));
                    notificacion.setTipo(c.getInt(colTipo));
                    notificacion.setContenido(c.getString(colMensaje));

                    Notificacions.add(notificacion);

                } while (c.moveToNext());
            }

            Close();

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return Notificacions;
    }

    @Override
    public Notificacion Find(Notificacion object) {
        throw new UnsupportedOperationException("Metodo no implementado");
    }

}
